class User < ApplicationRecord
  ## relações

  ## validações
    has_secure_password
    validates :name, :email, presence: true
    validates :password, :password_confirmation, length: {minimum:8, maximum:16}, :if => :password
    VALID_EMAIL_FORMAT= /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
    validates :email, uniqueness: {case_sensitive:true}, format: {with: VALID_EMAIL_FORMAT}, length:{maximum:100}
    before_save {self.email = email.downcase}

  ## funções

end
