Rails.application.routes.draw do
  ## Authentication
  post 'auth/login', to: 'session#login'
  post 'auth/sign_up', to: 'registration#signup'
  
  ## Funções de users
  resources :users, except: [:create]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
